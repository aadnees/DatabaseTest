var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "aadnees",
  password: "HnUcAoOT",
  database: "aadnees",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

test("update one person in db", done => {
    const id = 1;
    const navn = 'Hei Sveisen';
    const alder = 27;
    const adresse = 'Lerkendal';

    personDao.updateOne(id,{navn, alder, adresse}, (status, data) => {
        expect(data.affectedRows).toBe(1);
        personDao.getOne(id, (status, data) => {
            expect(data[0].navn === navn).toBeTruthy();
            expect(data[0].alder === alder).toBeTruthy();
            expect(data[0].adresse === adresse).toBeTruthy();
            done();
        });
    });
});


test("delete person from db", done =>{
    function callback(status, data) {
        console.log("status="+status + ", data="+JSON.stringify(data));
        expect(data.affectedRows).toBe(1);
        done();
    };

    personDao.deleteOne(24, callback);

});

/*test("delete person from db", done => {
    personDao.getAll((status, data) => {
        personDao.deleteOne(25, (status, data2) => {
            expect(data2.affectedRows).toBe(1);
            personDao.getAll((status, data3) => {
                expect(data3.length + 1 === data.length).toBe(false);
                done();
            });
            done();
        });
        done();
    });
});*/

